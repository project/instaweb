server.modules = ("mod_fastcgi","mod_magnet")

server.document-root = "<?php echo($root);?>"
server.port = <?php echo($port);?>

server.bind = "<?php echo($bind);?>"

index-file.names = ("index.php")
static-file.exclude-extensions = (".php")

include_shell "/usr/share/lighttpd/create-mime.assign.pl"

fastcgi.server += (".php" => ((
  "bin-path" => "/usr/bin/php-cgi",
  "socket" => "<?php echo($socketpath);?>",
  "max-procs" => 1,
  "idle-timeout" => 20,
  "bin-environment" => (
    "PHP_FCGI_CHILDREN" => "4",
    "PHP_FCGI_MAX_REQUESTS" => "10000"
  ),
  "bin-copy-environment" => (
    "PATH", "SHELL", "USER"
  ),
  "broken-scriptfilename" => "enable"
)))

magnet.attract-physical-path-to = ("<?php echo($magnetpath);?>")

