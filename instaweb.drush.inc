<?php

/**
 * @file
 *  Drush command for starting and stopping a webserver on localhost for
 *  development purposes.
 */

define('DRUSH_INSTAWEB_DEFAULT_BIND', 'localhost');
define('DRUSH_INSTAWEB_DEFAULT_PORT', 3000);
define('DRUSH_INSTAWEB_LIGHTY_CMD', '/usr/sbin/lighttpd');

/**
 * Implementation of hook_drush_command
 */
function instaweb_drush_command() {
  $items['instaweb'] = array(
    'description' => "Run a development webserver for this drupal instance",
    'options' => array(
      'bind' => 'Bind the testserver to this ip address. Default: ' . 
                DRUSH_INSTAWEB_DEFAULT_BIND,
      'port' => 'Use specified TCP port. Default: ' .
                DRUSH_INSTAWEB_DEFAULT_PORT,
      'server' =>  'Path to lighttpd binary. Default: ' .
                DRUSH_INSTAWEB_LIGHTY_CMD,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  return $items;
}

/**
 * Implementation of drush_hook_COMMAND_validate
 */
function drush_instaweb_validate() {
  $server = drush_get_option('server', DRUSH_INSTAWEB_LIGHTY_CMD);
  $bind = drush_get_option('bind', DRUSH_INSTAWEB_DEFAULT_BIND);
  $port = intval(drush_get_option('port', DRUSH_INSTAWEB_DEFAULT_PORT));

  if ($port < 1000 || $port > 0xFFFF) {
    return drush_set_error('PORT_OUT_OF_RANGE',
      dt('Please specify a port in the unprivileged range between 1000 and 65535'));
  }
}

/**
 * Implementation of drush_hook_COMMAND
 *
 * @TODO:
 *  * Install signal handler and cleanup after lighttpd exits
 */
function drush_instaweb() {
  $server = drush_get_option('server', DRUSH_INSTAWEB_LIGHTY_CMD);
  $bind = drush_get_option('bind', DRUSH_INSTAWEB_DEFAULT_BIND);
  $port = intval(drush_get_option('port', DRUSH_INSTAWEB_DEFAULT_PORT));

  // BEGIN ugly workaround to create temporary directory
  $tmpdir = sys_get_temp_dir();
  $mydir = tempnam($tmpdir, '');
  unlink($mydir);
  mkdir($mydir, 0700);
  if (!is_dir($mydir)) {
    return drush_set_error('CREATE_TMPDIR_FAILED',
      dt('Failed to create temporary directory for storing the configuration and the socket'));
  }
  // END ugly workaround to create temporary directory

  $configpath = $mydir . '/lighttpd.conf';
  $magnetpath = $mydir . '/drupal.lua';
  $socketpath = $mydir . '/php.socket';

  // write magnet.lua file
  $config = _instaweb_render_template('magnet.lua.tpl', array(
    'prefix' => $prefix,
  ));
  $magnetfd = fopen($magnetpath, "w");
  fwrite($magnetfd, $config);
  fclose($magnetfd);

  // write lighttpd.conf file
  $root = drush_get_context('DRUSH_DRUPAL_ROOT');
  $config = _instaweb_render_template('lighttpd.conf.tpl', array(
    'bind' => $bind,
    'port' => $port,
    'socketpath' => $socketpath,
    'magnetpath' => $magnetpath,
    'root' => $root,
  ));
  $configfd = fopen($configpath, "w");
  fwrite($configfd, $config);
  fclose($configfd);

  // start lighty
  drush_log(dt("Starting server on port !port", array("!port" => $port)), 'notice');
  drush_log(dt("Press ctl-C to exit"), 'notice');
  system($server . ' -D -f ' . $configpath);
}

/**
 * Render given template_file and return result. This function is a ripoff of
 * theme_render_template.
 */
function _instaweb_render_template($template_file, $variables) {
  extract($variables, EXTR_SKIP);               // Extract the variables to a local namespace
  ob_start();                                   // Start output buffering
  include $template_file;                       // Include the template file
  return ob_get_clean();                        // End buffering and return its contents
}
